# wist

Webdriving image search toolkit

## Testing

`py.test --cov-report html --cov=wist test/`

## Distributing

Make sure to have `~/.pip/pip.conf` setup with our jfrog repository 

```
python3 setup.py sdist bdist_wheel
python3 setup.py bdist_wheel upload -r local
```