from abc import ABC, abstractmethod
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import cv2
import io
import numpy as np
import json
import pytesseract
import os
from pathlib import Path


def get_viewport_dimensions(driver):
    try:
        vp_width = driver.execute_script("return document.documentElement.clientWidth")
        vp_height = driver.execute_script("return document.documentElement.clientHeight")
        return vp_width, vp_height
    except:
        return 0, 0


def get_viewport_offsets(driver):
    vp_offset_x = driver.execute_script("return window.pageXOffset")
    vp_offset_y = driver.execute_script("return window.pageYOffset")
    return vp_offset_x, vp_offset_y


def get_page_dimensions(driver):
    page_height = driver.execute_script("return document.body.clientHeight")
    page_width = driver.execute_script("return document.body.clientWidth")
    return page_width, page_height


class Image:
    @classmethod
    def take_screenshot(cls, driver):
        w, h = get_viewport_dimensions(driver)
        im = driver.get_screenshot_as_png()
        b = io.BytesIO(im)
        b.seek(0)
        img_array = np.asarray(bytearray(b.read()), dtype=np.uint8)
        img = cv2.imdecode(img_array, 0)
        (iw,ih) = img.shape[::-1]
        return Image(img, iw/w)

    @classmethod
    def from_file(cls, filename, pixel_ratio=1):
        file_path = Path(filename)
        if not file_path.exists():
            raise FileNotFoundError(f"Image file {filename} not found")
        img = cv2.imread(filename, 0)
        return Image(img, pixel_ratio)

    @classmethod
    def log_img(cls, img, named=""):
        if not os.path.exists("wist_log"):
            os.makedirs("wist_log")
        now = time.time()
        cv2.imwrite(f"wist_log/{now}_{named}.png", img)

    def __init__(self, img, pixel_ratio=1):
        self.initialize(img, pixel_ratio)

    def initialize(self, img, pixel_ratio):
        self.img = img
        dimensions = self.img.shape[::-1]
        self.width = dimensions[0]
        self.height = dimensions[1]
        self.pixel_ratio = pixel_ratio

    def save_img(self, filename):
        cv2.imwrite(filename, self.img)

    def to_pixel_ratio(self, new_ratio):
        if self.pixel_ratio == new_ratio:
            return

        new_image = cv2.resize(self.img, (0, 0), fx=new_ratio/self.pixel_ratio, fy=new_ratio/self.pixel_ratio, interpolation=cv2.INTER_AREA)
        self.initialize(new_image, new_ratio)


class TemplateImage():
    @classmethod
    def get_template(cls, repo_path, template_name):
        template_path = os.path.join(repo_path, template_name)
        with open(f"{template_path}.json", "r") as f:
            template_data = json.load(f)
        click_locations = {} if "points" not in template_data.keys() else template_data["points"]
        click_locations = {k: v["position"] for k, v in click_locations.items()}
        regions = {} if "regions" not in template_data.keys() else template_data["regions"]
        pixel_ratio = 1 if "pixelRatio" not in template_data.keys() else template_data["pixelRatio"]
        return TemplateImage(Image.from_file(f"{template_path}.png", pixel_ratio), click_locations, regions, template_name)

    def __init__(self, template_image, click_locations, regions, template_name):
        self.template_name = template_name
        self.template_image = template_image
        self.click_locations = click_locations
        self.regions = regions
        self.click_locations["center"] = [int(template_image.width/2), int(template_image.height/2)]

    def to_pixel_ratio(self, new_ratio):
        if new_ratio == self.template_image.pixel_ratio:
            return

        factor = new_ratio/self.template_image.pixel_ratio
        for k, v in self.click_locations.items():
            self.click_locations[k] = [int(self.click_locations[k][0] * factor), int(self.click_locations[k][1] * factor)]
        for k, v in self.regions.items():
            self.regions[k] = {"top_left": [int(v["top_left"][0] * factor), int(v["top_left"][1] * factor)],
                               "bottom_right": [int(v["bottom_right"][0] * factor), int(v["bottom_right"][1] * factor)]}
        self.template_image.to_pixel_ratio(new_ratio)

    def get_scaled_dimensions(self):
        pr = self.template_image.pixel_ratio
        return self.template_image.width / pr, self.template_image.height / pr


class TemplateMatch:
    @classmethod
    def _get_best_match_location(cls, img, template, debug=False):
        method = cv2.TM_CCOEFF_NORMED
        new_ratio = min(img.pixel_ratio, template.template_image.pixel_ratio)

        if img.pixel_ratio != new_ratio:
            if debug:
                print(f"Converting screen shot pixel_ratio {img.pixel_ratio}-->{new_ratio}")
            img.to_pixel_ratio(new_ratio)

        if template.template_image.pixel_ratio != new_ratio:
            if debug:
                print(f"Converting template pixel_ratio {template.template_image.pixel_ratio}-->{new_ratio}")
            template.to_pixel_ratio(new_ratio)

        res = cv2.matchTemplate(img.img, template.template_image.img, method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        return max_val, max_loc

    @classmethod
    def get_match(cls, img, template, driver, threshold, debug=False, log_diff=False):
        max_val, max_loc = TemplateMatch._get_best_match_location(img, template, debug)
        if debug:
            print(f"Template match: {template.template_name} @ {max_val}")
        ew, eh = get_viewport_dimensions(driver)
        vp_offset_x, vp_offset_y = get_viewport_offsets(driver)

        clickables = {}
        for k, v in template.click_locations.items():
            clickables[k] = [int((max_loc[0] + v[0])/img.width * ew) + vp_offset_x, int((max_loc[1] + v[1]) / img.height * eh) + vp_offset_y]
            if debug:
                print(f"{template.template_name}.{k}: ({max_loc[0]+v[0]}, {max_loc[1]+v[1]}) --> ([{img.width}, {img.height}] --> [{ew}, {eh}]) + [{vp_offset_x}, {vp_offset_y}] = {clickables[k]}")

        regions = {}
        for k, v in template.regions.items():
            regions[k] = {"top_left": [int(max_loc[0] + v["top_left"][0]),
                                       int(max_loc[1] + v["top_left"][1])],
                          "bottom_right": [int(max_loc[0] + v["bottom_right"][0]),
                                           int(max_loc[1] + v["bottom_right"][1])]}

        # Debug image building
        if debug:
            debug_img = cv2.cvtColor(img.img, cv2.COLOR_GRAY2RGB)
            bottom_right = (max_loc[0] + template.template_image.width, max_loc[1] + template.template_image.height)
            cv2.rectangle(debug_img, max_loc, bottom_right, [0, 0, 255], 2)
            cv2.putText(debug_img, f"{template.template_name} ({max_val})", (max_loc[0], max_loc[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
            for k, v in template.click_locations.items():
                click_coords = (int(max_loc[0] + v[0]), int(max_loc[1] + v[1]))
                cv2.circle(debug_img, click_coords, 5, (0, 255, 0), 2)
                cv2.putText(debug_img, k, (click_coords[0] + 10, click_coords[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
            for k, v in regions.items():
                cv2.rectangle(debug_img, tuple(v["top_left"]), tuple(v["bottom_right"]), [255, 0, 0], 2)
                cv2.putText(debug_img, k, (v["top_left"][0], v["top_left"][1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
            Image.log_img(debug_img, template.template_name.replace("/", "_"))

        if debug and log_diff:
            cropped = img.img[max_loc[1]:bottom_right[1], max_loc[0]:bottom_right[0]]
            diff = cv2.absdiff(cropped, template.template_image.img)
            Image.log_img(diff, template.template_name.replace("/", "_") + "_diff")

        if max_val > threshold:
            return TemplateMatch(driver, img, clickables, regions, debug=debug)
        else:
            return None

    def __init__(self, driver, screenshot, clickables, regions, debug=False):
        self.screenshot = screenshot
        self.clickables = clickables
        self.driver = driver
        self.regions = regions
        self.debug = debug

    def move_floater(self, location):
        self.driver.execute_script(
            f"""
            var d = document.getElementById('wistFloater');
            if (!d) {{
                d = document.createElement("div");
                d.id = 'wistFloater';
                d.style.position = "absolute";
                d.style.left = '0px';
                d.style.top = '0px';
                d.style.zIndex = '-100';
                document.getElementsByTagName("body")[0].appendChild(d);
            }}
            d.style.top = '{location[1]}px';
            d.style.left = '{location[0]}px'
            """)
        return self.driver.find_element_by_id("wistFloater")


    def click(self, name="center"):
        location = self.clickables[name]
        if self.debug:
            print(f"click: {location}")
        element = self.move_floater(location)
        webdriver.common.action_chains.ActionChains(self.driver).\
            move_to_element_with_offset(element, 0, 0).\
            click().\
            perform()

    def hover(self, hover_time, name="center"):
        location = self.clickables[name]
        if self.debug:
            print(f"hover: {location}")
        element = self.move_floater(location)
        webdriver.common.action_chains.ActionChains(self.driver).\
            move_to_element_with_offset(element, 0, 0).\
            pause(hover_time).\
            perform()

    def read_text(self, name):
        roi = self.regions[name]
        # Get to the croppa
        cropped = self.screenshot.img[roi["top_left"][1]:roi["bottom_right"][1], roi["top_left"][0]:roi["bottom_right"][0]]
        binary = cv2.threshold(cropped, 192, 255, cv2.THRESH_BINARY)
        new_img = cv2.resize(binary[1], (0,0), fx=4, fy=4)
        if self.debug:
            Image.log_img(new_img, "pre_tesseract_thresholded")
        return pytesseract.image_to_string(new_img)


class AbstractRepository(ABC):
    @abstractmethod
    def get_template(self, template_name):
        pass


class FileSystemRepository(AbstractRepository):
    def __init__(self, location="templates"):
        self.location = location

    def get_template(self, template_name):
        return TemplateImage.get_template(self.location, template_name)


class Wist:
    def __init__(self, driver, debug=False, repository=FileSystemRepository()):
        self.driver = driver
        self.debug = debug
        self.repository = repository

    def find_element(self, template_filename, timeout=5, threshold=0.9, debug=False):
        expiration_date = time.time() + timeout

        while expiration_date > time.time():
            img = Image.take_screenshot(self.driver)
            template = self.repository.get_template(template_filename)
            match = TemplateMatch.get_match(img, template, self.driver, threshold, debug=debug or self.debug)
            if match is not None:
                return match

        TemplateMatch.get_match(img, template, self.driver, threshold, log_diff=True, debug=debug or self.debug)
        raise TimeoutError

    def scroll_find_element(self, template_filename, threshold=0.9, debug=False):
        # TODO: This is only dealing with vertical scrolling for now, need to implement horizontal
        template = self.repository.get_template(template_filename)
        vp_width, vp_height = get_viewport_dimensions(self.driver)
        vp_offset_x, vp_offset_y = get_viewport_offsets(self.driver)
        page_width, page_height = get_page_dimensions(self.driver)
        template_dims = template.get_scaled_dimensions()
        assert template_dims[1] < vp_height
        scroll_increment = vp_height - template_dims[1]

        # Scroll to top of page
        self.driver.execute_script("window.scrollTo(0, 0)")
        while vp_offset_y + vp_height < page_height:
            if self.debug or debug:
                print(f"page_width: {page_width}, page_height: {page_height}, vp_width: {vp_width}, vp_height: {vp_height}, vp_offset_x: {vp_offset_x}, vp_offset_y: {vp_offset_y}")
            img = Image.take_screenshot(self.driver)
            match = TemplateMatch.get_match(img, template, self.driver, threshold, debug=debug or self.debug)
            if match is not None:
                return match
            # Scroll to next stop
            vp_offset_x, vp_offset_y = get_viewport_offsets(self.driver)
            self.driver.execute_script(f"window.scrollBy(0, {scroll_increment})")

        # TODO: need better error here
        raise FileNotFoundError
