import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="wist",
    version="0.0.3",
    author="Nak",
    author_email="",
    description="Webdriving Image Search Toolkit",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    install_requires=[
        "opencv-python",
        "pytesseract",
        "selenium"
    ]
)
