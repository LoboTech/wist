import pytest

import wist.wist as wist


class DriverMock():
    def __init__(self, viewport_width=400, viewport_height=400, offset_x=0, offset_y=0, page_height=400, page_width=400):
        self.responses = {
            "return document.documentElement.clientWidth": viewport_width,
            "return document.documentElement.clientHeight": viewport_height,
            "return window.pageXOffset": offset_x,
            "return window.pageYOffset": offset_y,
            "return document.body.clientHeight": page_height,
            "return document.body.clientWidth": page_width
        }
        self.execute_script = self.responses.get


@pytest.fixture()
def test_pattern():
    return wist.TemplateImage.get_template("test/resources", "test_pattern")


@pytest.fixture()
def test_pattern_pr1():
    return wist.TemplateImage.get_template("test/resources", "test_pattern_pr1")


@pytest.fixture()
def positive_image_pr2():
    return wist.Image.from_file("test/resources/search_image_positive_pr2.png", 2)


@pytest.fixture()
def negative_image_pr2():
    return wist.Image.from_file("test/resources/search_image_negative_pr2.png", 2)


@pytest.fixture()
def positive_image_pr1():
    return wist.Image.from_file("test/resources/search_image_positive_pr1.png", 1)


def test_match_location_with_target(test_pattern, positive_image_pr2):
    max_val, max_loc = wist.TemplateMatch._get_best_match_location(positive_image_pr2, test_pattern)
    assert max_val > .99
    assert max_loc == (100, 100)


def test_match_location_without_target(test_pattern, negative_image_pr2):
    max_val, max_loc = wist.TemplateMatch._get_best_match_location(negative_image_pr2, test_pattern)
    assert max_val < .1


def test_match_with_varying_pixel_ratios(test_pattern, positive_image_pr1):
    max_val, max_loc = wist.TemplateMatch._get_best_match_location(positive_image_pr1, test_pattern, True)
    assert max_val > .99
    assert max_loc == (50, 50)


def test_match_with_varying_pixel_ratios_2(test_pattern, positive_image_pr2):
    test_pattern.to_pixel_ratio(1)
    max_val, max_loc = wist.TemplateMatch._get_best_match_location(positive_image_pr2, test_pattern, True)
    assert max_val > .99
    assert max_loc == (50, 50)


def test_get_match_simple(test_pattern, positive_image_pr2):
    driver = DriverMock()
    match = wist.TemplateMatch.get_match(positive_image_pr2, test_pattern, driver, .99)
    assert len(match.clickables.keys()) == 2
    assert match.clickables["center"] == [200, 200]
    assert match.clickables["quad_1"] == [150, 150]

    assert len(match.regions) == 1
    assert match.regions["quad_4"] == {"top_left": [200, 200], "bottom_right": [300, 300]}


def test_get_match_scaled(test_pattern_pr1, positive_image_pr2):
    driver = DriverMock(viewport_width=200, viewport_height=200, page_height=200, page_width=200)
    match = wist.TemplateMatch.get_match(positive_image_pr2, test_pattern_pr1, driver, .99)
    assert len(match.clickables.keys()) == 2
    assert match.clickables["center"] == [100, 100]
    assert match.clickables["quad_1"] == [75, 75]

    assert len(match.regions) == 1
    assert match.regions["quad_4"] == {"top_left": [100, 100], "bottom_right": [150, 150]}


def test_get_match_no_match(test_pattern, negative_image_pr2):
    driver = DriverMock()
    match = wist.TemplateMatch.get_match(negative_image_pr2, test_pattern, driver, .99)
    assert match is None
