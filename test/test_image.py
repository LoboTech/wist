import wist.wist as wist
import pytest
import os
from pathlib import Path
import shutil


WIST_LOG_DIR = 'wist_log'


@pytest.fixture()
def test_pattern():
    """Returns the test pattern image with pixel ratio of 1"""
    return wist.Image.from_file("test/resources/test_pattern.png")


@pytest.fixture()
def double_ratio_pattern():
    """Returns the test pattern image with pixel ratio of 2"""
    return wist.Image.from_file("test/resources/test_pattern.png", 2)


@pytest.fixture()
def no_log_dir():
    """Removes the log dir if it exists"""
    if os.path.exists(WIST_LOG_DIR):
        shutil.rmtree(WIST_LOG_DIR)
    return None


def test_load_from_file_default_pixel_ratio(test_pattern):
    assert test_pattern.width == 200
    assert test_pattern.height == 200
    assert test_pattern.pixel_ratio == 1
    assert test_pattern.img is not None


def test_load_from_file_specified_pixel_ratio(double_ratio_pattern):
    assert double_ratio_pattern.pixel_ratio == 2


def test_load_invalid_file():
    with pytest.raises(FileNotFoundError):
        wist.Image.from_file("invalid_file.png")


def test_pixel_ratio_conversion(double_ratio_pattern):
    double_ratio_pattern.to_pixel_ratio(1)
    assert double_ratio_pattern.pixel_ratio == 1
    assert double_ratio_pattern.width == 100
    assert double_ratio_pattern.height == 100


def test_pixel_ratio_without_conversion(double_ratio_pattern):
    double_ratio_pattern.to_pixel_ratio(2)
    assert double_ratio_pattern.pixel_ratio == 2
    assert double_ratio_pattern.width == 200
    assert double_ratio_pattern.height == 200


def test_log_dir_creation(no_log_dir, test_pattern):
    assert not Path(WIST_LOG_DIR).exists()
    wist.Image.log_img(test_pattern.img, "test_img")
    assert Path(WIST_LOG_DIR).exists()


def test_save_image(test_pattern):
    if Path("test/resources/out_test.png").exists():
        os.remove("test/resources/out_test.png")
    test_pattern.save_img("test/resources/out_test.png")
    assert Path("test/resources/out_test.png").exists()
    os.remove("test/resources/out_test.png")
