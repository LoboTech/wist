import pytest
import wist.wist as wist


@pytest.fixture()
def test_pattern():
    """Return the test_pattern template"""
    return wist.TemplateImage.get_template("test/resources", "test_pattern")


@pytest.fixture()
def test_pattern_empty_json():
    return wist.TemplateImage.get_template("test/resources", "test_pattern_empty_json")


def test_get_invalid_template():
    with pytest.raises(FileNotFoundError):
        wist.TemplateImage.get_template("test/resources", "non_existent")


def test_get_template_empty_json(test_pattern_empty_json):
    template = test_pattern_empty_json
    assert template.template_name == "test_pattern_empty_json"
    assert template.template_image.height == 200
    assert template.template_image.width == 200
    assert template.template_image.pixel_ratio == 1
    assert len(template.click_locations) == 1
    assert template.click_locations["center"] == [100, 100]
    assert len(template.regions) == 0


def test_get_template(test_pattern):
    template = test_pattern
    assert template.template_name == "test_pattern"
    assert template.template_image.height == 200
    assert template.template_image.width == 200
    assert template.template_image.pixel_ratio == 2
    assert len(template.click_locations.keys()) == 2
    assert template.click_locations["center"] == [100, 100]
    assert template.click_locations["quad_1"] == [50, 50]
    assert len(template.regions) == 1
    assert template.regions["quad_4"]["top_left"] == [100, 100]
    assert template.regions["quad_4"]["bottom_right"] == [200, 200]


def test_pixel_ratio_conversion_not_needed(test_pattern):
    test_pattern.to_pixel_ratio(2)
    assert test_pattern.template_image.pixel_ratio == 2
    assert test_pattern.template_name == "test_pattern"
    assert test_pattern.template_image.height == 200
    assert test_pattern.template_image.width == 200
    assert test_pattern.template_image.pixel_ratio == 2
    assert len(test_pattern.click_locations.keys()) == 2
    assert test_pattern.click_locations["center"] == [100, 100]
    assert test_pattern.click_locations["quad_1"] == [50, 50]
    assert len(test_pattern.regions) == 1
    assert test_pattern.regions["quad_4"]["top_left"] == [100, 100]
    assert test_pattern.regions["quad_4"]["bottom_right"] == [200, 200]


def test_pixel_ratio_conversion(test_pattern):
    test_pattern.to_pixel_ratio(1)
    assert test_pattern.template_name == "test_pattern"
    assert test_pattern.template_image.height == 100
    assert test_pattern.template_image.width == 100
    assert test_pattern.template_image.pixel_ratio == 1
    assert len(test_pattern.click_locations.keys()) == 2
    assert test_pattern.click_locations["center"] == [50, 50]
    assert test_pattern.click_locations["quad_1"] == [25, 25]
    assert len(test_pattern.regions) == 1
    assert test_pattern.regions["quad_4"]["top_left"] == [50, 50]
    assert test_pattern.regions["quad_4"]["bottom_right"] == [100, 100]


def test_scaled_dimensions_with_scaling(test_pattern):
    assert test_pattern.get_scaled_dimensions() == (100, 100)


def test_scaled_dimension_without_scaling(test_pattern_empty_json):
    assert test_pattern_empty_json.get_scaled_dimensions() == (200, 200)
